package com.murcury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
public class MethodRepository {
	static WebDriver driver;
	static WebElement uname;
	static WebElement upass;
	static WebElement signin;
	static WebElement fromport;
	static WebElement findflights;
    static WebElement browse;
public static void browserLaunch() throws InterruptedException
{
	System.setProperty("webdriver.chrome.driver", "D:\\tools\\chromedriver.exe");
    driver=new ChromeDriver();
    driver.manage().window().maximize();
    Thread.sleep(3000);
}
public static void appLaunch() throws InterruptedException
{
	driver.get("http://newtours.demoaut.com/");	
	Thread.sleep(3000);
}
public static void validLogin() throws InterruptedException, AWTException
{
	uname=driver.findElement(By.name("userName"));
	uname.sendKeys("dasd");
	upass=driver.findElement(By.name("password"));
	upass.sendKeys("dasd");
	//signin=driver.findElement(By.name("login"));
	//signin=driver.findElement(By.xpath("//input[@value='Login']"));
	//signin.click();
	Robot r1 = new Robot();
	r1.keyPress(KeyEvent.VK_TAB);
	r1.keyRelease(KeyEvent.VK_TAB);
	r1.keyPress(KeyEvent.VK_ENTER);
	r1.keyRelease(KeyEvent.VK_ENTER);
	Thread.sleep(3000);	
	}

public static void loginusingSikiuli() throws FindFailed, InterruptedException
{
	uname=driver.findElement(By.name("userName"));
	uname.sendKeys("dasd");
	upass=driver.findElement(By.name("password"));
	upass.sendKeys("dasd");
	Screen sc1=new Screen();
	Pattern pt=new Pattern("D:\\tools\\Sikiuli\\2018-06-24_122653.png");
	sc1.click(pt);
	Thread.sleep(3000);	
}

public static void verifyValidlogin()
{
	String expectedtitle = "Sign-on: mercury Tours";
	String actualtitle = driver.getTitle();
	if(expectedtitle.equalsIgnoreCase(actualtitle))
	{
		System.out.println("Pass");
	}
	else
	{
		System.out.println("Fail");
	}
}


public static void departingFromselection()
{
	fromport=driver.findElement(By.name("fromPort"));
    //signin.click();
	Select s1=new Select(driver.findElement(By.name("fromPort")));
	//s1.selectByValue("Acapulco");
	s1.selectByIndex(3);
}
	public static void clickContinue() 
	{
		findflights	= driver.findElement(By.name("findFlights"));
		Actions a1=new Actions(driver);
		a1.moveToElement(findflights).click().perform();
	}	
		public static void newbrowserLaunch() throws InterruptedException
		{
			System.setProperty("webdriver.chrome.driver", "D:\\tools\\chromedriver.exe");
		    driver=new ChromeDriver();
		    driver.manage().window().maximize();
		    Thread.sleep(3000);
	}
	
public static void newappLaunch() throws InterruptedException, IOException
		{
			driver.get("https://upload.photobox.com/en/#computer");	
			Thread.sleep(3000);
			WebElement browse=driver.findElement(By.xpath("//button[@id='button_desktop']"));
			Actions a1=new Actions(driver);
			a1.moveToElement(browse).click().perform();
			Runtime.getRuntime().exec("D:\\tools\\Monta.exe");
		}
	
     
public static void appClose() throws InterruptedException
{
	driver.close();
	Thread.sleep(3000);}
}
